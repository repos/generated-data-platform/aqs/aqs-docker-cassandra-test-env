#!/bin/bash

# Set the repository URL
REPO_URL="https://gitlab.wikimedia.org/repos/sre/data-gateway.git"

# Set the directory name based on the repository name
REPO_DIR="data-gateway"

# Clone the repository if it does not exist
if [ ! -d "$REPO_DIR" ]; then
  git clone "$REPO_URL"
fi

# Navigate to the repository directory
cd "$REPO_DIR"

# Pull the latest changes
git pull

# Update the cassandra.go file with the environment variable configuration
cat <<EOL > cassandra.go
/*
 * Copyright 2022 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package main

import (
    "fmt"
    "os"
    "strconv"
    "strings"

    "github.com/gocql/gocql"
)

// Return a new Cassandra cluster config corresponding to the provided config.
func newCassandraCluster(config *Config) *gocql.ClusterConfig {
    hosts := os.Getenv("CASSANDRA_HOSTS")
    portStr := os.Getenv("CASSANDRA_PORT")
    port, _ := strconv.Atoi(portStr)

    cluster := gocql.NewCluster(strings.Split(hosts, ",")...)
    cluster.Consistency, _ = goCQLConsistency(config.Cassandra.Consistency)
    cluster.Port = port

    // Host selection policy
    if config.Cassandra.LocalDC != "" {
        dcAwarePolicy := gocql.DCAwareRoundRobinPolicy(config.Cassandra.LocalDC)
        cluster.PoolConfig.HostSelectionPolicy = gocql.TokenAwareHostPolicy(dcAwarePolicy)
    } else {
        cluster.PoolConfig.HostSelectionPolicy = gocql.RoundRobinHostPolicy()
    }

    // TLS
    tlsConf := config.Cassandra.TLS

    if tlsConf.CaPath != "" {
        cluster.SslOpts = &gocql.SslOptions{
            CaPath: tlsConf.CaPath,
        }
        cluster.SslOpts.CertPath = tlsConf.CertPath
        cluster.SslOpts.KeyPath = tlsConf.KeyPath
    }

    // Authentication
    authConf := config.Cassandra.Authentication

    if authConf.Username != "" {
        cluster.Authenticator = gocql.PasswordAuthenticator{
            Username: authConf.Username,
            Password: authConf.Password,
        }
    }

    return cluster
}

// Given a string, return the corresponding GoCQL consistency level type.
func goCQLConsistency(c string) (gocql.Consistency, error) {
    switch strings.ToLower(c) {
    case "any":
        return gocql.Any, nil
    case "one":
        return gocql.One, nil
    case "two":
        return gocql.Two, nil
    case "three":
        return gocql.Three, nil
    case "quorum":
        return gocql.Quorum, nil
    case "all":
        return gocql.All, nil
    case "localquorum":
        return gocql.LocalQuorum, nil
    case "eachquorum":
        return gocql.EachQuorum, nil
    case "localone":
        return gocql.LocalOne, nil
    default:
        return 0, fmt.Errorf("unrecognized Cassandra consistency level")
    }
}
EOL

# Create or update the Dockerfile if it does not exist or needs updating
cat <<EOL > Dockerfile
# Use the official Golang image as the base image
FROM golang:1.20-alpine

# Install curl
RUN apk add --no-cache curl

# Set the working directory inside the container
WORKDIR /app

# Copy the Go module files and download dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download

# Verify Go version
RUN go version

# Copy the rest of the application code
COPY . .

# Build the Go app
RUN go build -o /data-gateway

# Expose the port the app runs on
EXPOSE 8085

# Command to run the executable
CMD [ "/data-gateway" ]
EOL

# Use perl to find and replace the address in config.go
perl -pi -e 's/Address: "localhost"/Address: "0.0.0.0"/g' config.go

# Use perl to find and replace the listen_address in config.yaml
perl -pi -e 's/listen_address: localhost/listen_address: 0.0.0.0/g' config.yaml 

