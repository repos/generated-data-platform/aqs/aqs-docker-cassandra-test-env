#!/bin/bash

#editors by country query 
domain="analytics.wikimedia.org"
projects="('de.wikipedia', 'fr.wikipedia', 'en.wikipedia')"
activity_levels="('5..99-edits', '100..-edits')"
year_range="('2018', '2019', '2020', '2021')"
month_range=()    # to be filled later
output_filename="local_group_default_T_editors_bycountry.csv"

#fill month range
for i in 0{1..9} {10..12}; do
    month_range+=(\'${i}\')
done
IFS=", "
month_range="(${month_range[*]})"

printf "SELECT * FROM \"local_group_default_T_editors_bycountry\".data\
 WHERE \"_domain\" = '$domain' AND project IN $projects AND \"activity-level\" IN $activity_levels
 AND year IN $year_range AND month IN $month_range;" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename