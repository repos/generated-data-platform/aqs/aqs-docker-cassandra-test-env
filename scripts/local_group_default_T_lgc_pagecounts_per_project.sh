#!/bin/bash

# Legacy pagecounts query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access_sites="('all-sites', 'desktop-site', 'mobile-site')"
granularities="('monthly', 'daily', 'hourly')"
start_timestamp="20140101"
end_timestamp="20161231"
output_filename="local_group_default_T_lgc_pagecounts_per_project.csv"

printf "SELECT * FROM \"local_group_default_T_lgc_pagecounts_per_project\".data\
 WHERE \"_domain\" = '$domain' AND project = '$project'\
 AND \"access-site\" IN $access_sites AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename