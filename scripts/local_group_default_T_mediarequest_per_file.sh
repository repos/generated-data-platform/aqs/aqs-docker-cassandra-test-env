#!/bin/bash

# mediarequest per file query
domain="analytics.wikimedia.org"
referers="('all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
# %% -> escaped percent sign
file_paths="('/wikipedia/commons/1/1c/Manhattan_Bridge_Construction_1909.jpg', 
 '/wikipedia/commons/6/60/The_Earth_4K_Extended_Edition.webm', 
 '/wikipedia/commons/b/bd/Titan_(moon).ogg',
 '/wikipedia/commons/7/7e/NPS_craters-of-the-moon-map.pdf',
 '/wikipedia/commons/4/47/Catedral_de_la_Encarnaci%C3%B3n%2C_M%C3%A1laga%2C_Espa%C3%B1a%2C_2023-05-19%2C_DD_37-39_HDR.jpg',
 '/wikipedia/commons/0/0e/Angkor_-_Zentrum_des_K%C3%B6nigreichs_der_Khmer_(CC_BY-SA_4.0).webm',
 '/wikipedia/commons/e/ef/AB_Tacksfabriken_och_konf-fabr._AB_Viking._Trollh%C3%A4ttan_FiBs_serien_-_Nordiska_museet_-_NMAx.0001508.tif')"
granularities="('daily', 'monthly')"
start_timestamp="20180101"
end_timestamp="20231231"
output_filename="local_group_default_T_mediarequest_per_file.csv"

echo "SELECT * FROM \"local_group_default_T_mediarequest_per_file\".data 
 WHERE \"_domain\" = '$domain' AND referer IN $referers AND file_path IN $file_paths
 AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename
