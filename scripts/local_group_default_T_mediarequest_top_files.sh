#!/bin/bash

# top mediarequests query
domain="analytics.wikimedia.org"
referers="('all-referers', 'internal', 'external')"     # referers count has been reduced
media_types="('all-media-types', 'image', 'audio')"     # media_types count has been reduced
year="2019"     # full range: 2019-2020 (see below the second part of the script)         
month_range=()  # to be filled later
day_range=()    # to be filled later
output_filename="local_group_default_T_mediarequest_top_files.csv"

# fill month range
for i in 0{1..9} {10..12}; do
    month_range+=(\'${i}\')
done
IFS=", "
month_range="(${month_range[*]})"

# fill day range
for i in 0{1..9} {10..31} "all-days"; do
    day_range+=(\'${i}\')
done
IFS=", "
day_range="(${day_range[*]})"

# This script launch one query per year (we get a timetout when using IN operator)
printf "SELECT * FROM \"local_group_default_T_mediarequest_top_files\".data
 WHERE \"_domain\" = '$domain' AND referer IN $referers AND media_type IN $media_types
 AND year = '$year' AND month IN $month_range AND day IN $day_range;"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename

year="2020"
printf "SELECT * FROM \"local_group_default_T_mediarequest_top_files\".data
 WHERE \"_domain\" = '$domain' AND referer IN $referers AND media_type IN $media_types
 AND year = '$year' AND month IN $month_range AND day IN $day_range;"\
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
| sed '1d' \
>> $output_filename