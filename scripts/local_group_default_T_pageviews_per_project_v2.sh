#!/bin/bash

#Pageviews per project query
domain="analytics.wikimedia.org"
projects="('en.wikipedia', 'all-projects')"
access_sites="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
agents="('all-agents', 'automated', 'spider', 'user')"
granularities="('monthly', 'daily', 'hourly')"
start_timestamp="2018010100"
end_timestamp="2021123123"
output_filename="local_group_default_T_pageviews_per_project_v2.csv"

printf "SELECT * FROM \"local_group_default_T_pageviews_per_project_v2\".data\
 WHERE \"_domain\" = '$domain' AND project IN $projects AND access IN $access_sites
 AND agent IN $agents AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename