#!/bin/bash

# Unique devices query
domain="analytics.wikimedia.org"
projects="('en.wikipedia', 'lmo.wikipedia', 'bh.wikipedia', 'ta.wiktionary', 'it.wikibooks', 'commons.wikimedia', 'all-wikipedia-projects')"
access_sites="('all-sites', 'desktop-site', 'mobile-site')"
granularities="('monthly', 'daily')"
output_filename="local_group_default_T_unique_devices.csv"
start_timestamp="20180101"
end_timestamp="20211231"

printf "SELECT * FROM \"local_group_default_T_unique_devices\".data\
 WHERE \"_domain\" = '$domain' AND project IN $projects\
 AND \"access-site\" IN $access_sites AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename